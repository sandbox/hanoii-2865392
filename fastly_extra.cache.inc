<?php

/**
 * @file fastly_extra.cache.inc
 *
 * Dummy cache bin handler to purge all fastly cache on drupal cache clear.
 */

class FastlyCache implements DrupalCacheInterface {
  function get($cid) {
    return FALSE;
  }

  function getMultiple(&$cids) {
    return array();
  }

  function set($cid, $data, $expire = CACHE_PERMANENT) {
  }

  function clear($cid = NULL, $wildcard = FALSE) {
    if ($wildcard && $cid == '*') {
      $api = fastly_get_api();
      $api->purgeAll();
    }
  }

  function isEmpty() {
    return FALSE;
  }
}
